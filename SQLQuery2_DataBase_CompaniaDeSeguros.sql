﻿USE CompaniaDeSeguros
GO



 CREATE PROCEDURE Insertar_Usuario
 @NUsuario VARCHAR(50),
 @Contra VARCHAR(50),
 @Rol VARCHAR(50)
 AS

 INSERT INTO Usuario (NUsuario, Contra, Rol, Estado) 
	VALUES (@NUsuario, ENCRYPTBYPASSPHRASE(@Contra, @Contra), @Rol, 'Habilitado')

EXECUTE Insertar_Usuario 'Christian', 'poli', 'Administrador'
EXECUTE Insertar_Usuario 'Alberto', 'admin', 'Administrador'


CREATE PROCEDURE Validar_Acceso
@NUsuario VARCHAR(50),
@Contra VARCHAR(50)
AS
DECLARE @Resultado VARCHAR(50)
IF EXISTS (SELECT TOP 1 * FROM Usuario
			WHERE NUsuario = @NUsuario AND DECRYPTBYPASSPHRASE(@Contra, Contra) = @Contra
			AND Estado = 'Habilitado')
			BEGIN

			SET @Resultado = 'Acceso Exitoso'
			END
			ELSE
			SET @Resultado = 'Acceso Denegado'

			SELECT @Resultado AS Resultado 

			SELECT * FROM Usuario

			EXECUTE Validar_Acceso 'Christian', 'poli'
			EXECUTE Validar_Acceso 'Alberto', 'admin'