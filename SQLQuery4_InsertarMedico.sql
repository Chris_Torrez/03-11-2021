﻿USE CompaniaDeSeguros
GO

DROP PROCEDURE Insertar_Medico

CREATE PROCEDURE Insertar_Medico
@primerNombreM VARCHAR(15),
@segundoNombreM VARCHAR(15),
@primerApellidoM VARCHAR(15),
@segundoApellidoM VARCHAR(15)
AS

INSERT INTO Medico ([Primer Nombre], [Segundo Nombre], [Primer Apellido], [Segundo Apellido])
VALUES (@primerNombreM, @segundoNombreM, @primerApellidoM, @segundoApellidoM)

SELECT * FROM Medico