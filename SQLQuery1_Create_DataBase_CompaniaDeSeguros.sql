﻿CREATE LOGIN Compania
WITH PASSWORD = 'company'
GO

USE CompaniaDeSeguros
GO

sp_adduser Compania, SistemasCJS
sp_addrolemember db_owner, SistemasCJS

SELECT * FROM Usuario