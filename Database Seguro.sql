Create database Seguro
go
use Seguro
go

--Sp_adduser Boo,Boo
--sp_addrolemember db_Owner,Boo

--
Create table Vendedor
(IdVendedor int primary key identity(1,1),
[Primer Nombre] varchar(50) not null,
[Segundo Nombre] varchar (50) null,
[Primer Apellido] varchar(50)not null,
[Segundo Apellido] varchar(50) null)
--
Create table [Poliza de Seguro]
(NoPoliza int primary key identity(1,1),
[Tipo Poliza] varchar(50) not null,
[Costo de Poliza] float  not null,
[Costo por Extensio] float not null,
[Valor de Cobertura] float not null)

--
Create table Asegurado
(NoAsegurado int primary key identity(1,1),
[Primer Nombre] varchar(50) not null,
[Segundo Nombre] varchar (50) null,
[Primer Apellido] varchar(50)not null,
[Segundo Apellido] varchar(50) null,
Direccion varchar(50) not null)
--
Create table Contrato
(IdContrato int primary key identity(1,1),
NoAsegurado int not null,
NoPoliza int not null,
IdVendedor int not null,
[Fecha de Contratacion] date not null,
[Fecha inicial de Vigencia] date null,----
[Fecha final de Vigencia] date null)----

Alter table Contrato
Add foreign key (NoAsegurado) References Asegurado(NoAsegurado)
Alter table Contrato
Add foreign key (NoPoliza) References [Poliza de Seguro](NoPoliza)
Alter table Contrato
Add foreign key (IdVendedor) References Vendedor(IdVendedor)

--
Create table [Familiar Asegurado](
IdFamiliar int primary key identity(1,1),
[Primer Nombre] varchar(50) not null,
[Segundo Nombre] varchar (50) null,
[Primer Apellido] varchar(50)not null,
[Segundo Apellido] varchar(50) null,
Edad int null,
[Fecha de Nacimiento] date not null,
Direccion varchar(50) not null)
--
Create table Extension(
IdContrato int not null,
IdFamiliar int not null,
[Costo de Extension] float null)----

Alter table Extension
Add foreign key (IdContrato) References Contrato(IdContrato)
Alter table Extension
Add foreign key (IdFamiliar) References [Familiar Asegurado](IdFamiliar)

Alter table Extension
Add primary key (IdContrato,IdFamiliar)
--
Create table Medico
(IdMedico int primary key identity(1,1),
[Primer Nombre] varchar(50) not null,
[Segundo Nombre] varchar (50) null,
[Primer Apellido] varchar(50)not null,
[Segundo Apellido] varchar(50) null)
--
Create table Hospital
(IdHospital int primary key identity(1,1),
Red varchar (50) not null,
Nombre varchar(50) not null,
[Numero de Apartamentos] int not null,
Direccion varchar(50) not null,
Telefono varchar(50) not null)
--
Create table Hospitalizacion
(NoHospitalizacion int primary key identity(1,1),
NoAsegurado int not null,
IdHospital int not null,
IdMedico int not null,
[Fecha de Entrada] date null,
[Fecha de Salida] date null,
Gasto float null)

Alter table Hospitalizacion
Add foreign key (NoAsegurado) References Asegurado(NoAsegurado)
Alter table Hospitalizacion
Add foreign key (IdHospital) References Hospital(IdHospital)
Alter table Hospitalizacion
Add foreign key (IdMedico) References Medico(IdMedico)

--
Create table Costo
(IdCosto int primary key identity(1,1),
NoHospitalizacion int not null,
Descripcion varchar (50),
Costo float null) 

Alter table Costo
Add foreign key (NoHospitalizacion) References Hospitalizacion(NoHospitalizacion)