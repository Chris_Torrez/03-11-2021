﻿USE CompaniaDeSeguros
GO

CREATE PROCEDURE Insertar_Vendedor
@primernombre VARCHAR(15),
@segundonombre VARCHAR(15),
@primerapellido VARCHAR(15),
@segundoapellido VARCHAR(15)
AS

INSERT INTO Vendedor ([Primer Nombre], [Segundo Nombre], [Primer Apellido], [Segundo Apellido])
VALUES (@primernombre, @segundonombre, @primerapellido, @segundoapellido)