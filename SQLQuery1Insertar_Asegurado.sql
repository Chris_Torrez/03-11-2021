﻿USE CompaniaDeSeguros
GO
CREATE PROCEDURE Insertar_Asegurado
@primerNombre VARCHAR(15),
@segundoNombre VARCHAR(15),
@primerApellido VARCHAR(15),
@segundoApellido VARCHAR(15)
AS
INSERT INTO Asegurado ([Primer Nombre], [Segundo Nombre], [Primer Apellido],[Segundo Apellido]) 
VALUES (@primerNombre, @segundoNombre, @primerApellido, @segundoApellido)

DROP PROCEDURE Insertar_Asegurado
