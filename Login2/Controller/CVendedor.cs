﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Login2.Data;
using Login2.Views;
using System.Data;

namespace Login2.Controller
{
    class CVendedor
    {
        public static DataTable MostrarVendedor()
        {
            Window7_dtVendedor wind7 = new Window7_dtVendedor();
            return DVendedor.MostrarVendedor();
        }
        public static string InsertarVendedor(string p_nombre, string s_nombre, string p_apellido, string s_apellido)
        {
            DVendedor Obj = new DVendedor();
            Obj.P_nombreV = p_nombre;
            Obj.S_nombreV = s_nombre;
            Obj.P_apellidoV = p_apellido;
            Obj.S_apellidoV = s_apellido;
            return Obj.Insertar(Obj);

        }


    }
}
