﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Login2.Data;
using Login2.Views;

namespace Login2.Controller
{
    class CMedico
    {
        public static string InsertarMedico(string p_nombre, string s_nombre, string p_apellido, string s_apellido)
        {
            DMedico Obj = new DMedico();
            Obj.P_nombreM = p_nombre;
            Obj.S_nombreM = s_nombre;
            Obj.P_apellidoM = p_apellido;
            Obj.S_apellidoM = s_apellido;

            return Obj.InsertarMedico(Obj);
        }

        public static DataTable MostrarAsegurado()
        {
            Window5dtMedico wind5 = new Window5dtMedico();
            return DMedico.MostrarMedico();
        }
    }
}
