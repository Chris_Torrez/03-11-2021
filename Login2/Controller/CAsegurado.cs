﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Login2.Data;
using Login2.Views;

namespace Login2.Controller
{
    class CAsegurado
    {
        public static DataTable MostrarAsegurado()
        {
            Window3dtAsegurado wind3 = new Window3dtAsegurado();
            return DAsegurado.MostrarAsegurado();
        }
        public static string Insertar(string p_nombre, string s_nombre, string p_apellido, string s_apellido, string direccion)
        {
            DAsegurado Obj = new DAsegurado();
            Obj.P_nombre = p_nombre;
            Obj.S_nombre = s_nombre;
            Obj.P_apellido = p_apellido;
            Obj.S_apellido = s_apellido;
            Obj.Direccion = direccion;
            return Obj.Insertar(Obj);
        }

        public static string Editar(string p_nombre, string s_nombre, string p_apellido, string s_apellido, string direccion)
        {
            DAsegurado Obj = new DAsegurado();
            Obj.P_nombre = p_nombre;
            Obj.S_nombre = s_nombre;
            Obj.P_apellido = p_apellido;
            Obj.S_apellido = s_apellido;
            Obj.Direccion = direccion;
            return Obj.Editar(Obj);

        }


       
    }
}
