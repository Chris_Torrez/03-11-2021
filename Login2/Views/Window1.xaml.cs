﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Login2.Views
{
    /// <summary>
    /// Lógica de interacción para Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
            //secondGrid.RenderTransform = new ScaleTransform(-50, 50);
        }

        private void logoutBtn_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void closeMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            openMenuBtn.Visibility = Visibility.Visible;
            closeMenuBtn.Visibility = Visibility.Collapsed;
        }

        private void openMenuBtn_Click(object sender, RoutedEventArgs e)
        {
            openMenuBtn.Visibility = Visibility.Collapsed;
            closeMenuBtn.Visibility = Visibility.Visible;
        }

   
        private void InsertarAsegBtn_Click(object sender, RoutedEventArgs e)
        {
            Window2Insertar ventana = new Window2Insertar();
            ventana.ShowDialog();
        }

        private void InsertarMedBtn_Click(object sender, RoutedEventArgs e)
        {
            Window4InsertarMedico ventana = new Window4InsertarMedico();
            ventana.ShowDialog();

        }

        private void InsertarVendBtn_Click(object sender, RoutedEventArgs e)
        {
            Window6_InsertarVendedor ventana = new Window6_InsertarVendedor();
            ventana.ShowDialog();
        }
    }
}
