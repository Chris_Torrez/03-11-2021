﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Login2.Controller;

namespace Login2.Views
{
    /// <summary>
    /// Lógica de interacción para Window6_InsertarVendedor.xaml
    /// </summary>
    public partial class Window6_InsertarVendedor : Window
    {
        private bool IsNuevo = true;
        public Window6_InsertarVendedor()
        {
            InitializeComponent();
        }
        private void insertarVBtn_Click(object sender, RoutedEventArgs e)
        {

            try
            {

                string respuesta = "";

                if (this.IsNuevo)
                {

                    respuesta = CVendedor.InsertarVendedor(this.primerNombreVTxt.Text, this.segundoNombreVTxt.Text, this.primerApellidoVTxt.Text, this.segundoApellidoVTxt.Text);

                }

                if (respuesta == CVendedor.InsertarVendedor(this.primerNombreVTxt.Text, this.segundoNombreVTxt.Text, this.primerApellidoVTxt.Text, this.segundoApellidoVTxt.Text))
                {

                    if (this.IsNuevo)
                    {
                        MessageBox.Show("Datos ingresados", "Compañía de seguros", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Datos no ingresados", "Compañía de seguros", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }



            }
            catch (Exception)
            {

            }

        }
        private void mostrarVBtn_Click(object sender, RoutedEventArgs e)
        {
            dtVen.DataContext = CVendedor.MostrarVendedor();
            dtVen.ShowDialog();
        }
        private void minBtnVe_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void closeBtnVe_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }



        Window7_dtVendedor dtVen = new Window7_dtVendedor();
    }
}
