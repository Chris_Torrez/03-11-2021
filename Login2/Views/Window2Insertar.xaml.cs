﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Login2.Data;
using Login2.Controller;
using Login2.Views;

namespace Login2.Views
{
    /// <summary>
    /// Lógica de interacción para Window2Insertar.xaml
    /// </summary>
    public partial class Window2Insertar : Window
    {
        private bool IsNuevo = true;
        //private bool IsEditar = false;
        public Window2Insertar()
        {
            InitializeComponent();
        }



        private void insertarBtn_Click(object sender, RoutedEventArgs e)
        {
           
            try
            {
                
                string respuesta = "";

              if (this.IsNuevo)
                {
                    
                    respuesta = CAsegurado.Insertar(this.primerNombreTxt.Text, this.segundoNombreTxt.Text, this.primerApellidoTxt.Text, this.segundoApellidoTxt.Text, this.direccionTxt.Text);
                    
                }

              if (respuesta== CAsegurado.Insertar(this.primerNombreTxt.Text, this.segundoNombreTxt.Text, this.primerApellidoTxt.Text, this.segundoApellidoTxt.Text, this.direccionTxt.Text))
                {
                    
                    if (this.IsNuevo)
                    {
                        MessageBox.Show("Datos ingresados", "Compañía de seguros", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Datos no ingresados", "Compañía de seguros", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                   

               
            }
            catch (Exception)
            {

            }
        }

        private void mostrarBtn_Click(object sender, RoutedEventArgs e)
        {
            
          
            dtAseg.DataContext = CAsegurado.MostrarAsegurado();
            dtAseg.ShowDialog();
            
        }

        private void editarBtn_Click(object sender, RoutedEventArgs e)
        {
            
            if (this.dtAseg.dtAsegurado.SelectedIndex != -1)
            {
                this.primerNombreTxt.Text = Convert.ToString(dtAseg.dtAsegurado.CurrentCell.Column.Header);
            }
        }



        Window3dtAsegurado dtAseg = new Window3dtAsegurado();

        private void minBtn_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;

        }

        private void closeBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            
        }
    }
}
