﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Login2.Controller;
using Login2.Views;

namespace Login2.Views
{
    /// <summary>
    /// Lógica de interacción para Window4InsertarMedico.xaml
    /// </summary>
    public partial class Window4InsertarMedico : Window
    {
        public Window4InsertarMedico()
        {
            InitializeComponent();
        }

        private bool IsNuevo = true;
        //private bool IsEditar = false;

        private void insertarMBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                string respuesta = "";

                if (this.IsNuevo)
                {

                    respuesta = CMedico.InsertarMedico(this.primerNombreMTxt.Text, this.segundoNombreMTxt.Text, this.primerApellidoMTxt.Text, this.segundoApellidoMTxt.Text);

                }

                if (respuesta == CMedico.InsertarMedico(this.primerNombreMTxt.Text, this.segundoNombreMTxt.Text, this.primerApellidoMTxt.Text, this.segundoApellidoMTxt.Text))
                {

                    if (this.IsNuevo)
                    {
                        MessageBox.Show("Datos ingresados", "Compañía de seguros", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        MessageBox.Show("Datos no ingresados", "Compañía de seguros", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }



            }
            catch (Exception)
            {

            }

        }

        private void mostrarMBtn_Click(object sender, RoutedEventArgs e)
        {
            dtMed.DataContext = CMedico.MostrarAsegurado();
            dtMed.ShowDialog();
        }

        private void minBtnMe_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void closeBtnMe_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        Window5dtMedico dtMed = new Window5dtMedico();
    }
}
