﻿#pragma checksum "..\..\..\Views\Window2Insertar.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "8E327E1F94C70862FAE53AB1992196821836959B2885FB4AB10EE6F00ED043EB"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using Login2.Views;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Login2.Views {
    
    
    /// <summary>
    /// Window2Insertar
    /// </summary>
    public partial class Window2Insertar : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\..\Views\Window2Insertar.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Formulario;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\Views\Window2Insertar.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox primerNombreTxt;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\Views\Window2Insertar.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox segundoNombreTxt;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\Views\Window2Insertar.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox primerApellidoTxt;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\Views\Window2Insertar.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox segundoApellidoTxt;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\Views\Window2Insertar.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox direccionTxt;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\Views\Window2Insertar.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button minBtn;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\Views\Window2Insertar.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button closeBtn;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\Views\Window2Insertar.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button insertarBtn;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\..\Views\Window2Insertar.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button editarBtn;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\Views\Window2Insertar.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button mostrarBtn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Login2;component/views/window2insertar.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\Window2Insertar.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Formulario = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.primerNombreTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.segundoNombreTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.primerApellidoTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.segundoApellidoTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.direccionTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.minBtn = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\..\Views\Window2Insertar.xaml"
            this.minBtn.Click += new System.Windows.RoutedEventHandler(this.minBtn_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.closeBtn = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\..\Views\Window2Insertar.xaml"
            this.closeBtn.Click += new System.Windows.RoutedEventHandler(this.closeBtn_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.insertarBtn = ((System.Windows.Controls.Button)(target));
            
            #line 46 "..\..\..\Views\Window2Insertar.xaml"
            this.insertarBtn.Click += new System.Windows.RoutedEventHandler(this.insertarBtn_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.editarBtn = ((System.Windows.Controls.Button)(target));
            
            #line 55 "..\..\..\Views\Window2Insertar.xaml"
            this.editarBtn.Click += new System.Windows.RoutedEventHandler(this.editarBtn_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.mostrarBtn = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\..\Views\Window2Insertar.xaml"
            this.mostrarBtn.Click += new System.Windows.RoutedEventHandler(this.mostrarBtn_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

