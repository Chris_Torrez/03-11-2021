﻿#pragma checksum "..\..\..\Views\Window6_InsertarVendedor.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "1BBF62FD26C3DA4E83E5B5C8E464577C128600153A48BDA902A4E2579D203BD7"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using Login2.Views;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Login2.Views {
    
    
    /// <summary>
    /// Window6_InsertarVendedor
    /// </summary>
    public partial class Window6_InsertarVendedor : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 13 "..\..\..\Views\Window6_InsertarVendedor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid Formulario;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\Views\Window6_InsertarVendedor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox primerNombreVTxt;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\Views\Window6_InsertarVendedor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox segundoNombreVTxt;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\Views\Window6_InsertarVendedor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox primerApellidoVTxt;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\Views\Window6_InsertarVendedor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox segundoApellidoVTxt;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\..\Views\Window6_InsertarVendedor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button minBtnVe;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\Views\Window6_InsertarVendedor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button closeBtnVe;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\Views\Window6_InsertarVendedor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button insertarVBtn;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\Views\Window6_InsertarVendedor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button editarVBtn;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\..\Views\Window6_InsertarVendedor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button mostrarVBtn;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Login2;component/views/window6_insertarvendedor.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\Window6_InsertarVendedor.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Formulario = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.primerNombreVTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.segundoNombreVTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.primerApellidoVTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.segundoApellidoVTxt = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.minBtnVe = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\..\Views\Window6_InsertarVendedor.xaml"
            this.minBtnVe.Click += new System.Windows.RoutedEventHandler(this.minBtnVe_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.closeBtnVe = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\..\Views\Window6_InsertarVendedor.xaml"
            this.closeBtnVe.Click += new System.Windows.RoutedEventHandler(this.closeBtnVe_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.insertarVBtn = ((System.Windows.Controls.Button)(target));
            
            #line 49 "..\..\..\Views\Window6_InsertarVendedor.xaml"
            this.insertarVBtn.Click += new System.Windows.RoutedEventHandler(this.insertarVBtn_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.editarVBtn = ((System.Windows.Controls.Button)(target));
            return;
            case 10:
            this.mostrarVBtn = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\..\Views\Window6_InsertarVendedor.xaml"
            this.mostrarVBtn.Click += new System.Windows.RoutedEventHandler(this.mostrarVBtn_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

