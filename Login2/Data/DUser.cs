﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Login2.Data
{
    class DUser
    {
        int idUsuario;
        string usuario;
        string contraseña;
        string rol;

        public int IdUsuario { get => idUsuario; set => idUsuario = value; }
        public string Usuario { get => usuario; set => usuario = value; }
        public string Contraseña { get => contraseña; set => contraseña = value; }
        public string Rol { get => rol; set => rol = value; }

        public static DataTable Validar_Acceso(string user, string contra)
        {
            DataTable DtResultado = new DataTable("Inicio_Sesión");

            SqlConnection SqlCon = new SqlConnection();

            try
            {    
                SqlCon.ConnectionString = Connection.conexion; // Cargando la conexión del servidor

                // Creando un objeto SQLCommand que llamará al procedimiento almacenado
                SqlCommand SqlCmd = new SqlCommand();

                SqlCmd.Connection = SqlCon;

                SqlCmd.CommandText = "Validar_Acceso";

                SqlCmd.CommandType = CommandType.StoredProcedure;

                //   Cargando los parámetros del procedimiento almacenado
                SqlParameter ParUsuario = new SqlParameter();

                ParUsuario.ParameterName = "@NUsuario";

                ParUsuario.SqlDbType = SqlDbType.VarChar;

                ParUsuario.Size = 60;

                ParUsuario.Value = user;

                SqlCmd.Parameters.Add(ParUsuario);



                SqlParameter ParContraseña = new SqlParameter();

                ParContraseña.ParameterName = "@Contra";

                ParContraseña.SqlDbType = SqlDbType.VarChar;

                ParContraseña.Size = 100;

                ParContraseña.Value = contra;

                SqlCmd.Parameters.Add(ParContraseña);

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);

                SqlDat.Fill(DtResultado);

            }
            catch (Exception ex)
            {
                DtResultado = null;

                Console.WriteLine(ex.Message);
            }
            return DtResultado;

        }

        public DataTable Mostrar_Usuario()
        {
            DataTable DtResultado = new DataTable("MostrarUsuario");
            SqlConnection SqlCon = new SqlConnection();

            try
            {
                //Cargando la conexión al servidor
                SqlCon.ConnectionString = Connection.conexion;
                //Creando objeto SQLcommand que llamará al procedimiento almacenado
                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Mostrar_Usuario";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter SqlDat = new SqlDataAdapter(SqlCmd);
                SqlDat.Fill(DtResultado);
            }
            catch (Exception e)
            {
                DtResultado = null;
            }

            return DtResultado;
        }

        public string InsertarUsuario (DUser usuario)
        {
            string respuesta = "";
            SqlConnection SqlCon = new SqlConnection();

            try
            {
                SqlCon.ConnectionString = Connection.conexion;
                SqlCon.Open();

                SqlCommand SqlCmd = new SqlCommand();
                SqlCmd.Connection = SqlCon;
                SqlCmd.CommandText = "Insertar_Usuario";
                SqlCmd.CommandType = CommandType.StoredProcedure;

                //Parámetros del procedimiento almacenado

                SqlParameter ParPrimerNombre = new SqlParameter();
                ParPrimerNombre.ParameterName = "@usuario";
                ParPrimerNombre.SqlDbType = SqlDbType.VarChar;
                ParPrimerNombre.Size = 50;
                ParPrimerNombre.Value = usuario.Usuario;
                SqlCmd.Parameters.Add(ParPrimerNombre);

                SqlParameter ParPrimerApellido = new SqlParameter();
                ParPrimerApellido.ParameterName = "@rol";
                ParPrimerApellido.SqlDbType = SqlDbType.VarChar;
                ParPrimerApellido.Size = 50;
                ParPrimerApellido.Value = usuario.Rol;
                SqlCmd.Parameters.Add(ParPrimerApellido);

                //Ejecutando el comando
                respuesta = SqlCmd.ExecuteNonQuery() == 1 ? "Ok" : "No ingresó el registro";

            }
            catch (Exception e)
            {
                respuesta = e.Message;
            }
            finally
            {
                if (SqlCon.State == ConnectionState.Open) SqlCon.Close();
            }
            return respuesta;
        }

    }
}
