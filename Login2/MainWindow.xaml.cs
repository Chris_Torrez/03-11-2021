﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.Data;
using Login2.Controller;
using Login2.Views;

namespace Login2
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
           


            //string conexion = ConfigurationManager.ConnectionStrings["Login2.Properties.Settings.CompaniaDeSegurosConnectionString"].ConnectionString;
        }

        private void aceptarBtn_Click(object sender, RoutedEventArgs e)
        {

            DataTable dato;
            dato = CUser.Validar_Acceso(this.usuarioTxtBox.Text, this.contraTxtBox.Password);

            if (dato != null)
            {

                if (dato.Rows.Count > 0)
                {
                    DataRow dr;
                    dr = dato.Rows[0];
                    if (dr["Resultado"].ToString() == "Acceso Exitoso")
                    {
                        Console.WriteLine("Ha ingresado");
                       


                        MessageBox.Show("Bienvenido al Sistema", "Sistema de seguros", MessageBoxButton.OK, MessageBoxImage.Information);


                        Window1 VentanaPrincipal = new Window1();
                        //VentanaPrincipal.Owner = this;
                        VentanaPrincipal.ShowDialog();

                    }
                    else
                    {
                        MessageBox.Show("Acceso Denegado al Sistema de seguros", "Sistema de seguros", MessageBoxButton.OK, MessageBoxImage.Error);
                        this.usuarioTxtBox.Text = string.Empty;
                        this.contraTxtBox.Password = string.Empty;
                        this.usuarioTxtBox.Focus();
                    }
                }
            }
            else
                MessageBox.Show("No hay conexión al servidor", "Sistema de seguros", MessageBoxButton.OK, MessageBoxImage.Error);


        }

        
        

       /* private void usuarioTxtBox_TextChanged(object sender, TextChangedEventArgs e)
        {

            if (usuarioTxtBox.Text == "") 
            {
                // Create an ImageBrush.
                ImageBrush textImageBrush = new ImageBrush();
                textImageBrush.ImageSource =
                    new BitmapImage(
                        new Uri(@"Usernamegi.gif", UriKind.Relative)
                    );
                textImageBrush.AlignmentX = AlignmentX.Left;
                textImageBrush.Stretch = Stretch.None;
                // Use the brush to paint the button's background.
                usuarioTxtBox.Background = textImageBrush;
            }
            else
            {

                usuarioTxtBox.Background = null;
            }

        }*/

        private void cancelarBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            
        }

        private void minimizarBtn_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void salirBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
